# Library

[Para ver como o projeto foi feito, clique aqui.](https://www.youtube.com/@ThiagoCorgosinho/videos)


Para iniciar essa aplicação siga estes passos:

* Rode o 'bundle' no projeto

* Configure o banco de dados no 'database.yml'

* Rode um db:create e db:migrate

* Prontinho, pode rodar o console usando rails c ou o server com rails s

