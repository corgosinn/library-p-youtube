class AddNameToBookcase < ActiveRecord::Migration[7.0]
  def change
    add_column :bookcases, :name, :string
  end
end
